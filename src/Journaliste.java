public class Journaliste {

    private int id;
    private String pseudo;
    private int credit;

    /* creer notre methode journaliste que nous allons appeler dans notre classe DAO*/
    public Journaliste(int identifiant, String pseudoJournaliste, int creditAccorde) {

        this.id = identifiant;
        this.pseudo = pseudoJournaliste;
        this.credit = creditAccorde;

    }

    public int getId() {
        return id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public int getCredit() {
        return credit;
    }

    @Override
    public String toString() {
        return "Journaliste{" +
                "id=" + id +
                ", pseudo='" + pseudo + '\'' +
                ", credit=" + credit +
                '}';
    }
}
