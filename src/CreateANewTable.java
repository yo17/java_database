import java.sql.*;


public class CreateANewTable {



    public static void table(int id){

         final String CREATE_TABLE_SQL="CREATE TABLE employee ("
                + "EMP_ID int(11) NOT NULL,"
                + "NAME VARCHAR(45) NOT NULL,"
                + "DOB DATE NOT NULL,"
                + "EMAIL VARCHAR(45) NOT NULL,"
                + "DEPT varchar(45) NOT NULL,"
                + "PRIMARY KEY (EMP_ID))";

        PreparedStatement stmt = null;

        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/backlog_ifp", "root", "");
            System.out.println("Connection Established");



            stmt = connection.prepareStatement(CREATE_TABLE_SQL);
            stmt.executeUpdate();

            System.out.println("Table created");

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } /*finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
    }
}
