import java.util.Date;

public class News {

    private int id;
    private String titre;
    private String contenue;
    private Date date;
    private String tag;

        /* creer notre methode journaliste que nous allons appeler dans notre classe DAO*/
        public News(int identifiant, String titre, String contenue , Date date, String tag) {

            this.id = identifiant;
            this.titre = titre;
            this.contenue= contenue;
            this.date = date;
            this.tag = tag;

        }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", contenue='" + contenue + '\'' +
                ", date=" + date +
                ", tag='" + tag + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getContenue() {
        return contenue;
    }

    public Date getDate() {
        return date;
    }

    public String getTag() {
        return tag;
    }
}


